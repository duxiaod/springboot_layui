SpringBoot+redis+thymeleaf+MyBatis+LayUI+极验。整合开发

# spring_layui 0.1

数据库文件在db目录下

## 介绍
基础整合

页面使用了layui，验证码：极验

## 项目特点
1. 基于SpringBoot+redis+thymeleaf+MyBatis+LayUI+极验

## 项目截图
![登陆](https://images.gitee.com/uploads/images/2018/0818/005007_be84b6c8_1045447.png "11.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0818/005142_312de271_1045447.png "22.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0818/005153_d9eb7c35_1045447.png "33.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0818/005203_cd40f060_1045447.png "44.png")