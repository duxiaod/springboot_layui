/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50549
Source Host           : localhost:3306
Source Database       : springboot_layui

Target Server Type    : MYSQL
Target Server Version : 50549
File Encoding         : 65001

Date: 2018-08-18 00:26:45
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` varchar(64) NOT NULL,
  `login_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(64) DEFAULT NULL,
  `login_ip` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `create_by` varchar(64) NOT NULL,
  `update_by` varchar(64) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `remarks` varchar(255) DEFAULT NULL,
  `login_flag` char(1) DEFAULT '0',
  `del_flag` char(1) NOT NULL DEFAULT '0',
  `hold1` varchar(255) DEFAULT NULL,
  `hold2` varchar(255) DEFAULT NULL,
  `hold3` varchar(255) DEFAULT NULL,
  `hold4` varchar(255) DEFAULT NULL,
  `hold5` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('e10adc3949ba59abbe56e057f20f883e', 'admin', 'admin', 'arthur', '593151321@', '17693109997', '127.0.0.1', null, 'e10adc3949ba59abbe56e057f20f883e', 'e10adc3949ba59abbe56e057f20f883e', '2018-08-17 17:20:03', '2018-08-17 17:20:09', null, '0', '0', null, null, null, null, null);
