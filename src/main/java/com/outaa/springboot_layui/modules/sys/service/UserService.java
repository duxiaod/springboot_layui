package com.outaa.springboot_layui.modules.sys.service;

import com.outaa.springboot_layui.comm.service.CrudService;
import com.outaa.springboot_layui.modules.sys.dao.UserDao;
import com.outaa.springboot_layui.modules.sys.entity.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService extends CrudService<UserDao,User> {
    public List<User> getUserByLoginName(User user) {
        return dao.getUserByLoginName(user);
    }
}