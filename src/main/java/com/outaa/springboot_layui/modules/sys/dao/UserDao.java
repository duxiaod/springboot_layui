package com.outaa.springboot_layui.modules.sys.dao;

import com.outaa.springboot_layui.comm.dao.CrudDao;
import com.outaa.springboot_layui.modules.sys.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface UserDao extends CrudDao<User> {
    List<User> getUserByLoginName(User user);
}
